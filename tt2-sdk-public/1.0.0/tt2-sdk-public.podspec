Pod::Spec.new do |spec|

  spec.name         = "tt2-sdk-public"
  spec.version      = "1.0.0"
  spec.summary      = "Indoor positioning for iOS."
  spec.description  = "Software development kit for using the advanced indoor positioning system developed by Virtual Stores."
  spec.homepage     = "http://virtualstores.se"
  spec.author       = {
    "Jesper Lundqvist" => "jesper.lundqvist@virtualstores.se",
    "Théodore Roos" => "theodore.roos@virtualstores.se",
    "Emil Bond" => "emil.bond@virtualstores.se",
    "Philip Fryklund" => "philip.fryklund@virtualstores.se",
    "Carl-Johan Dahlman" => "carljohan.dahlman@virtualstores.se"
  }
  spec.license      = "Proprietary"
  spec.source       = { :git => 'https://bitbucket.org/virtualstores/tt2-sdk-public.git', :tag => "#{spec.version}" }
  spec.public_header_files = "TT2SDK.framework/Headers/*.h"
  spec.source_files = "TT2SDK.framework/Headers/*.h"
  spec.vendored_frameworks = "TT2SDK.framework"
  spec.static_framework = true

  # Platform
  spec.platform              = :ios
  spec.ios.deployment_target = "12.0"
  spec.swift_version         = "5.0"
end
