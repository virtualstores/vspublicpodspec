Pod::Spec.new do |spec|
  spec.name         = "PositionKit"
  
  spec.version      = "1.0.62"

  spec.summary      = "A short description of PositionKit."
  spec.description  = "A kit to get some positions"
  spec.homepage     = "http://virtualstores.se"
  spec.license      = "MIT"
  spec.author       = { "Bond" => "emil.bond@virtualstores.se" }
  spec.source       = { :git => "http://bitbucket.org/virtualstores/PositionKitPublic.git", :tag => "#{spec.version}" }

  spec.platform     = :ios
  spec.ios.deployment_target = "11.0"

  spec.source_files  = "Sources", "Sources/**/*.{h,m,swift}"
  spec.exclude_files = "Classes/Exclude"

  spec.vendored_frameworks  = "qps.framework"
end
